<?php

namespace PlayerStatsBundle\Domain;

class PlayerName
{
    private $playerMap = [
        '76561197962348102' => 'King Julien',
        '76561198201068321' => 'papipl2',
        '76561198081157069' => 'papipl|koza',
        '76561198007692950' => 'taniquetil',
        '76561198013657121' => 'f4il',
        '76561197969697265' => 'Vobo',
        '76561198050742988' => 'furball',
    ];

    private $name;

    public function __construct($playerId)
    {
        $this->name = $this->playerMap[$playerId];
    }

    public function name()
    {
        return $this->name;
    }
}
